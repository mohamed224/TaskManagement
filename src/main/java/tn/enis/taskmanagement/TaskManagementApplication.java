package tn.enis.taskmanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import tn.enis.taskmanagement.entities.Task;
import tn.enis.taskmanagement.entities.User;
import tn.enis.taskmanagement.service.AppService;
import tn.enis.taskmanagement.service.TaskService;

@SpringBootApplication
public class TaskManagementApplication implements CommandLineRunner {

	@Autowired
	TaskService taskService;
	@Autowired
	AppService appService;

	public static void main(String[] args) {
		SpringApplication.run(TaskManagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/*User u1 = new User("Mohamed", "1234");
		User u2 = new User("Jean", "1234");
		appService.saveUser(u1);
		appService.saveAdmin(u2);

		taskService.saveTask(new Task("Task1", "This is my 1st task", null, null), u1);
		taskService.saveTask(new Task("Task 2", "This is my 2nd task", null, null), u2);
*/
		// System.out.println(t1.getCreatedDate());

		/*
		 * taskService.getAllTask().forEach(task->{
		 * taskService.updateTaskStartDate(task, task.getId());
		 * taskService.updateTaskEndDate(task, task.getId());
		 * System.out.println(task.getStartDate()+"   "+task.getEndDate()); });
		 */
		
		User u1 = new User("Mohamed", "1234");
		User u2 = new User("Jean", "1234");
		appService.saveUser(u1);
		appService.saveAdmin(u2);
		System.out.println(appService.findUserByUsername("Mohamed").getUsername());
		taskService.saveTask(new Task("Task1", "This is my 1st task"), u1);
		taskService.saveTask(new Task("Task 2", "This is my 2nd task"), u2);

	}

	@Bean
	PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

}
