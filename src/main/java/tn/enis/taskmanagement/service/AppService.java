package tn.enis.taskmanagement.service;

import java.util.List;

import tn.enis.taskmanagement.entities.User;


public interface AppService {
	
	public User saveUser(User user);
	public User saveAdmin(User user);
//	public List<User> findUserByUsername(String userName);
	public User findUserByUsername(String username);
	public List<User> findAll();
	

}
