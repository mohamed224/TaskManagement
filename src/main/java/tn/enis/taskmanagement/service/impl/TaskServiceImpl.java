package tn.enis.taskmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import tn.enis.taskmanagement.entities.Task;
import tn.enis.taskmanagement.entities.User;
import tn.enis.taskmanagement.repository.TaskRepository;
import tn.enis.taskmanagement.service.TaskService;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	TaskRepository taskRepository;

	@Override
	public Task saveTask(Task task,User user) {
		task.setUser(user);
		return taskRepository.save(task);
	}

	@Override
	public List<Task> getAllTask() {
		return taskRepository.findAll();
	}

	@Override
	public Page<Task> getTaskByUser(User user , Pageable pageable) {
		return taskRepository.findByUser(user , pageable);
	}

	@Override
	public Task updateTask(Task task, Long id) {
		task.setId(id);
		return taskRepository.save(task);

	}

	@Override
	public void deleteTask(Long id) {
		taskRepository.deleteById(id);

	}


}
