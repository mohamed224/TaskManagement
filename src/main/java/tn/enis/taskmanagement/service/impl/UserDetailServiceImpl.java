package tn.enis.taskmanagement.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import tn.enis.taskmanagement.entities.User;
import tn.enis.taskmanagement.security.JWTAuthenticationFilter;
import tn.enis.taskmanagement.service.AppService;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

	private static final Logger LOG = LoggerFactory.getLogger(JWTAuthenticationFilter.class);
	@Autowired
	AppService appService;

	@Override
	public UserDetails loadUserByUsername(String username) {
		User user = appService.findUserByUsername(username);
		if (user == null) {
			LOG.error("User not found");
			throw new UsernameNotFoundException("User not found");

		}
		return getUserCredentials(user);
	}

	public org.springframework.security.core.userdetails.User getUserCredentials(User user) {
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				getAuthorities(user));
	}

	public Collection<GrantedAuthority> getAuthorities(User user) {
		Collection<GrantedAuthority> authorites = new ArrayList<>();
		user.getRoles().forEach(r -> authorites.add(new SimpleGrantedAuthority(r.getRoleName())));
		return authorites;
	}

}
