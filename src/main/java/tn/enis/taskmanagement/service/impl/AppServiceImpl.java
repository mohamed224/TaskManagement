package tn.enis.taskmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import tn.enis.taskmanagement.entities.Role;
import tn.enis.taskmanagement.entities.User;
import tn.enis.taskmanagement.repository.RoleRepository;
import tn.enis.taskmanagement.repository.UserRepository;
import tn.enis.taskmanagement.service.AppService;

@Service
public class AppServiceImpl implements AppService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	PasswordEncoder encoder;
	
	AppServiceImpl(PasswordEncoder encoder){
		this.encoder = encoder;
	}

	@Override
	public User saveUser(User user) {
		user.setPassword(encoder.encode(user.getPassword()));
		Role role = new Role("USER");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		user.setRoles(roles);
		return userRepository.save(user);
	}

	@Override
	public User saveAdmin(User user) {
		user.setPassword(encoder.encode(user.getPassword()));
		Role role = new Role("ADMIN");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		user.setRoles(roles);
		return userRepository.save(user);
	}

/*	@Override
	public List<User> findUserByUsername(String userName) {
		return userRepository.findByUsernameLike("%" + userName + "%");
	}*/

	@Override
	public User findUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

}
