package tn.enis.taskmanagement.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tn.enis.taskmanagement.entities.Task;
import tn.enis.taskmanagement.entities.User;

public interface TaskService {

	public Task saveTask(Task task, User user);

	public List<Task> getAllTask();

	public Page<Task> getTaskByUser(User user , Pageable pageable);

	public Task updateTask(Task task, Long id);

	public void deleteTask(Long id);

}
