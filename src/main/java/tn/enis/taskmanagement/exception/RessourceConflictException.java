package tn.enis.taskmanagement.exception;

public class RessourceConflictException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public RessourceConflictException(String message) {
		super(message);
	}
	
	public RessourceConflictException(String message , Throwable cause) {
		super(message,cause);
	}
	
	public RessourceConflictException(Throwable cause) {
		super(cause);
	}
	

}
