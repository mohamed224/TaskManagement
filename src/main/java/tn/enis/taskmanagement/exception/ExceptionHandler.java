package tn.enis.taskmanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> handleAllException(Exception ex) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(500, ex.getMessage());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(RessourceConflictException.class)
	public final ResponseEntity<ExceptionResponse> handleRessourceConflictException(Exception ex) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(409, ex.getMessage());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.CONFLICT);
	}

}
