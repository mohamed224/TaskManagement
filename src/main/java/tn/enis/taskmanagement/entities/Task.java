package tn.enis.taskmanagement.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonBackReference;

import tn.enis.taskmanagement.audit.Auditable;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Task extends Auditable<String> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message = "This title field is required")
	private String title;
	@Column(length = 1000)
	@NotBlank(message = "This description field is required")
	private String description;
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Task() {
		super();
	}

	public Task(String title, String description,  User user) {
		super();
		this.title = title;
		this.description = description;
		this.user = user;
	}

	public Task(String title, String description) {
		super();
		this.title = title;
		this.description = description;

	}

}
