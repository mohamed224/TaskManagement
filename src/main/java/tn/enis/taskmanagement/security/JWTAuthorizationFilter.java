package tn.enis.taskmanagement.security;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

	private final TokenProperties tokenProperties;

	public JWTAuthorizationFilter(TokenProperties tokenProperties) {
		this.tokenProperties = tokenProperties;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String header = request.getHeader(tokenProperties.getHeader());
		 if (headerIsValid(header)) {
	            try {
	                Claims claims = getClaims(getToken(header));
	                Optional.ofNullable(claims.getSubject())
	                        .ifPresent(username -> setUserContext(claims, username));
	            } catch (Exception e) {
	                SecurityContextHolder.clearContext();
	            }
	        }

	        goToNextFilter(request, response, filterChain);

	}

	private boolean headerIsValid(String header) {
		return header != null && header.startsWith(tokenProperties.getPrefix());
	}

	private String getToken(String header) {
		return header.replace(tokenProperties.getPrefix(), "");
	}

	private Claims getClaims(String token) {
		return Jwts.parser().setSigningKey(tokenProperties.getSecret().getBytes()).parseClaimsJws(token).getBody();
	}

	 private void setUserContext(Claims claims, String username) {
	    
	        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
	                username,
	                null,
	                getGrantedAuthorities(claims)
	        );
	        SecurityContextHolder.getContext().setAuthentication(auth);
	    }
	@SuppressWarnings("unchecked")
	private List<SimpleGrantedAuthority> getGrantedAuthorities(Claims claims) {
		return ((List<String>) claims.get("authorities")).stream().map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
	}

	private void goToNextFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			FilterChain filterChain) throws IOException, ServletException {
		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

}
