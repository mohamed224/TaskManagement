package tn.enis.taskmanagement.security;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import tn.enis.taskmanagement.entities.User;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private static final Logger LOG = LoggerFactory.getLogger(JWTAuthenticationFilter.class);
	private ObjectMapper objectMapper;
	private final AuthenticationManager authenticationManager;
	private final TokenProperties tokenProperties;
	

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, TokenProperties tokenProperties) {
		super();
		this.authenticationManager = authenticationManager;
		this.tokenProperties = tokenProperties;
		objectMapper = new ObjectMapper();
		setFilterProcessesUrl(tokenProperties.getLoginPath());
	}

	
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			User credentials = getUserCredentials(request);
			LOG.info(credentials.getUsername()+"  "+credentials.getPassword());
			UsernamePasswordAuthenticationToken authenticationToken = createAuthenticationToken(credentials);
			return authenticationManager.authenticate(authenticationToken);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	private User getUserCredentials(HttpServletRequest request) throws IOException {
		return objectMapper.readValue(request.getInputStream(), User.class);
	}
	
	private UsernamePasswordAuthenticationToken createAuthenticationToken(User credentials) {
		return new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword(), Collections.emptyList());
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		response.addHeader(tokenProperties.getHeader(),tokenProperties.getPrefix()+createToken(authResult));
	}
	
	private String createToken(Authentication auth) {
		long now = System.currentTimeMillis();
		List<String> authorites = auth.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList());
		return Jwts.builder()
				.setSubject(auth.getName())
				.claim("authorities", authorites)
				.setIssuedAt(new Date(now))
				.setExpiration(new Date(now+tokenProperties.getExpiration()))
				.signWith(SignatureAlgorithm.HS512, tokenProperties.getSecret().getBytes())
				.compact();
				
	}

}
