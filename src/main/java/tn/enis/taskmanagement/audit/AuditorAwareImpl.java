package tn.enis.taskmanagement.audit;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwareImpl implements AuditorAware<String>{

	Logger LOG = LoggerFactory.getLogger(AuditorAwareImpl.class);
	@SuppressWarnings("unchecked")
	@Override
	public Optional<String> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication == null) {
			return Optional.of("Mohamed");
		}
		LOG.info(authentication.getName()+"---"+authentication.getPrincipal()+"---"+authentication.getAuthorities().toString());
		return Optional.of(authentication.getName());
	}
	

}
