package tn.enis.taskmanagement.utils;

import javax.validation.constraints.NotBlank;

public class FormData {

	@NotBlank(message="Veuillez renseigner ce champs")
	private String username;
	@NotBlank(message="Veuillez renseigner ce champs")
	private String password;
	@NotBlank(message="Veuillez renseigner ce champs")
	private String repassword;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

}
