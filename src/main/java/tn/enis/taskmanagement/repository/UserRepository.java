package tn.enis.taskmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.enis.taskmanagement.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
	
	List<User> findByUsernameLike(String username);

	User findByUsername(String username);

}
