package tn.enis.taskmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.enis.taskmanagement.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	public Role findByRolename(String rolename);

}
