package tn.enis.taskmanagement.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.enis.taskmanagement.entities.Task;
import tn.enis.taskmanagement.entities.User;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
	
	Page<Task> findByUser(User user , Pageable pageable);

}
