package tn.enis.taskmanagement.config;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import tn.enis.taskmanagement.security.JWTAuthenticationFilter;
import tn.enis.taskmanagement.security.JWTAuthorizationFilter;
import tn.enis.taskmanagement.security.TokenProperties;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true,securedEnabled=true,jsr250Enabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private UserDetailsService userDetailsService;
	private PasswordEncoder encoder;
	

	SecurityConfig(UserDetailsService userDetailsService , PasswordEncoder encoder){
		this.userDetailsService = userDetailsService;
		this.encoder = encoder;
	}
	
	
	@Autowired
	private TokenProperties tokenProperties;
	
	
	public static final long MAX_AGE = 3600L;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(daoAuthenticationProvider());
		//auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
	
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.authorizeRequests().antMatchers("/api/users/**" ).permitAll()
				.and().authorizeRequests().antMatchers(HttpMethod.POST, "/api/login").permitAll()
				.and().authorizeRequests().antMatchers( "/api/tasks/**").permitAll()
				.anyRequest().authenticated()
				.and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager(), tokenProperties))
				.addFilterBefore(
						new JWTAuthorizationFilter(tokenProperties), UsernamePasswordAuthenticationFilter.class);
	}

	 @Bean
	 public CorsConfigurationSource corsConfigurationSource() {

	        CorsConfiguration configuration = new CorsConfiguration();
	        configuration.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
	        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
	        configuration.setAllowedHeaders(Arrays.asList("x-requested-with", "authorization" ,
	                "Content-Type", "Authorization", "credential", "X-XSRF-TOKEN", "Content-Disposition"));
	        configuration.setAllowCredentials(true);
	        configuration.setExposedHeaders(Arrays.asList("Access-Control-Allow-Origin",
	                "Access-Control-Allow-Credentials","Authorization"));
	        configuration.setMaxAge(MAX_AGE);
	        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	        source.registerCorsConfiguration("/**", configuration);
	        return source;
	    }
	 
	DaoAuthenticationProvider daoAuthenticationProvider() {
		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setPasswordEncoder(encoder);
		daoAuthenticationProvider.setUserDetailsService(this.userDetailsService);
		 return daoAuthenticationProvider;
	 }
	 
	
	
}
