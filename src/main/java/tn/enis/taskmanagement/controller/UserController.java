package tn.enis.taskmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.taskmanagement.entities.User;
import tn.enis.taskmanagement.exception.RessourceConflictException;
import tn.enis.taskmanagement.service.AppService;
import tn.enis.taskmanagement.utils.FormData;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class UserController {

	@Autowired
	public AppService appService;
	
	
	@PostMapping("/users")
	public User signUp(@Valid @RequestBody FormData data) throws RessourceConflictException {
		String username = data.getUsername();
		String password = data.getPassword();
		String repassword = data.getRepassword();
		User user = appService.findUserByUsername(username);
		if(user!=null) {
			throw new RessourceConflictException("User already exist");
		}
		if(!password.equals(repassword)) {
			throw new RessourceConflictException("The passwords must be the same");
		}
		User u = new User(username, password);
		return appService.saveUser(u);
	}
	
	@GetMapping("/users")
	public List<User> getAllUsers(){
		return appService.findAll();
	}
	
	
}
