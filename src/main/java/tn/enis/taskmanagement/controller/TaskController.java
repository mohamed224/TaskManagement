package tn.enis.taskmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.taskmanagement.entities.Task;
import tn.enis.taskmanagement.entities.User;
import tn.enis.taskmanagement.service.AppService;
import tn.enis.taskmanagement.service.TaskService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class TaskController {

	@Autowired
	TaskService taskService;

	@Autowired
	AppService appService;

	@PostMapping("/tasks/{username}")
	public Task saveTask(@Valid @RequestBody Task task, @PathVariable(value = "username") String username) {
		User user = appService.findUserByUsername(username);
		return taskService.saveTask(task, user);
	}

	@GetMapping("/tasks/{username}")
	public Page<Task> getTaskByUser(@PathVariable(value = "username") String username,
			@RequestParam(name = "page") int page, @RequestParam(name = "size", defaultValue = "5") int size) {
		User user = appService.findUserByUsername(username);
		return taskService.getTaskByUser(user , PageRequest.of(page, size));
	}

	@GetMapping("/tasks")
	public List<Task> getAllTask() {
		return taskService.getAllTask();
	}

	@PutMapping("/tasks/{id}")
	public Task updateTask(@Valid @RequestBody Task task, @PathVariable(value = "id") Long id) {
		return taskService.updateTask(task, id);
	}

	@DeleteMapping("/tasks/{id}")
	public void deleteTask(@PathVariable(value = "id") Long id) {
		taskService.deleteTask(id);
	}
}
