package tn.enis.taskmanagement.constants;

public class SecurityConstants {

	public static final String AUTH_LOGIN_URL = "/api/authenticate";
	public static final String JWT_SECRET = "mdiaby00@gmail.com";
	public static final String TOKEN_HEADER = "Authorization";
	public static final String TOKEN_PREFIX = "Bearer";
	public static final String TOKEN_TYPE = "JWT";
	public static final long EXPIRATION_TIME = 864_000_000;
}
