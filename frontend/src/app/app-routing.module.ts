import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './authentication/login/login.component';
import {NewTaskComponent} from './task/new-task/new-task.component';
import {TasksComponent} from './task/tasks/tasks.component';
import {TaskDetailsComponent} from './task/task-details/task-details.component';
import {RegisterComponent} from './users/register/register.component';
import {UserComponent} from './users/user/user.component';
import {AuthGuardGuard} from './authentication/auth-guard.guard';
import {TaskResolver} from './task/shared/service/task-resolver';


const routes: Routes = [

  {
    path : 'login' , component : LoginComponent,
  },
  {
    path : 'register' , component : RegisterComponent
  },
  {
    path : 'tasks' , component : TasksComponent , canActivate : [AuthGuardGuard] ,resolve : {
      tasks : TaskResolver
    }
  },
  {
    path: 'newTask' , component : NewTaskComponent , canActivate : [AuthGuardGuard]
  },
  {
    path : 'taskDetails/:id' , component : TaskDetailsComponent , canActivate : [AuthGuardGuard]
  },
  {
    path: 'profile' , component : UserComponent , canActivate : [AuthGuardGuard]
  },
  {
    path : '' , redirectTo : 'tasks' , pathMatch : 'full' , canActivate : [AuthGuardGuard]
  },
  {
    path : '**' , component : TasksComponent , canActivate : [AuthGuardGuard]
  }
]
@NgModule({
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
