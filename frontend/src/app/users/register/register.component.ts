import {Component, OnInit} from '@angular/core';
import {RegisterData} from '../shared/model/register-data';
import {UserService} from '../shared/service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  errorMessage : any
  mode=1
  registerData : RegisterData = new RegisterData()
  constructor(private userService : UserService) { }

  ngOnInit() {
  }

  register(){
    this.userService.register(this.registerData)
      .subscribe(resp=>{
        this.registerData = new RegisterData()
        this.mode = 1
        console.log(resp)
      },err=>{
        this.mode = 0
        this.errorMessage = err.error.message
        console.log(err.error)
      })

  }

}
