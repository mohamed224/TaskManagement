import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RegisterData} from '../model/register-data';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = "http://localhost:8083/api/"
  constructor(private http : HttpClient) { }

  public register(registerData : RegisterData) : Observable<any>{
      return this.http.post<any>(this.apiUrl+"users" , registerData);
  }
}

