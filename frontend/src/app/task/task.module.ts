import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewTaskComponent} from './new-task/new-task.component';
import {TaskDetailsComponent} from './task-details/task-details.component';
import {TasksComponent} from './tasks/tasks.component';
import {SearchPipePipe} from './shared/pipe/search-pipe.pipe';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    NewTaskComponent,
    TaskDetailsComponent,
    TasksComponent,
    SearchPipePipe
  ]
})
export class TaskModule { }
