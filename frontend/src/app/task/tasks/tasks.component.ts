import {Component, OnInit} from '@angular/core';
import {TaskService} from '../shared/service/task.service';
import {Task} from '../shared/model/task';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  tasks : any = new Array();
  currentPage : number = 0
  size : number = 5
  pages : any = new Array()
  searchText : string = ""
  constructor(private taskService : TaskService , private router : Router , private route : ActivatedRoute) { }



  ngOnInit() {
    this.tasks = this.route.snapshot.data['tasks']
    this.pages =new Array(this.tasks.totalPages)
    this.tasks = this.tasks.content

  }

  getTaskByUser(){
    this.taskService.getTaskByUser(this.currentPage , this.size).subscribe(resp=>{
      this.tasks = resp.content
      this.pages =new Array(resp.totalPages)
      //console.log(resp)
    })
  }
  editTask(task : Task){

  }

  deleteTask(task : Task) {
    this.taskService.deleteTask(task.id)
      .subscribe(resp=> {
        this.tasks.splice(this.tasks.indexOf(task),1)

      })
  }

  detailTask(id : number) {

  }

  gotoPage(i : number){
    this.currentPage = i
    this.getTaskByUser()
  }

}
