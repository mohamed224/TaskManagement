import {Pipe, PipeTransform} from '@angular/core';
import {Task} from '../model/task';

@Pipe({
  name: 'searchPipe'
})
export class SearchPipePipe implements PipeTransform {

  transform(tasks: Task [], searchText: string): Task [] {
    if(searchText === null || searchText === "") {
      return tasks
    }
    return tasks.filter(task=>task.title.includes(searchText )|| task.description.includes(searchText))
  }

}
