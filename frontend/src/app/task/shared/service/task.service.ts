import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Task} from '../model/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  apiUrl = "http://localhost:8083/api/"
  constructor(private http : HttpClient) { }

  public getTaskByUser(page : number , size : number) : Observable<any>{
    let username = localStorage.getItem('username')
      return this.http.get(this.apiUrl+"tasks/"+username+"?page="+page+"&size="+size)
  }


  public saveTask(task : Task) : Observable<any> {
    let username = localStorage.getItem('username')
    return this.http.post(this.apiUrl+"tasks/"+username, task)
  }

  public updateTask(task : Task) : Observable<any> {
    let id = task.id
    return this.http.put(this.apiUrl+"tasks/"+id , task)
  }

  public deleteTask(id : number) : Observable<any> {
    return this.http.delete(this.apiUrl+"tasks/"+id)
  }
}
