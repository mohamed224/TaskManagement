import {Resolve} from '@angular/router';
import {TaskService} from './task.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskResolver implements Resolve<any>{

  constructor(private taskService : TaskService){}

  resolve(){
    return this.taskService.getTaskByUser(0,5);
  }

}
