import {Component, OnInit} from '@angular/core';
import {Task} from '../shared/model/task';
import {TaskService} from '../shared/service/task.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit {

  task : Task = new Task()
  constructor(private taskService : TaskService , private router : Router) { }

  ngOnInit() {
  }

  saveTask(){
    this.taskService.saveTask(this.task)
      .subscribe(resp=>{
        this.router.navigateByUrl('tasks')
        console.log(resp)
      })
  }
}
