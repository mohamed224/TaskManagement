import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthenticationService} from './shared/service/authentication.service';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor{

  constructor(private authService : AuthenticationService , private router : Router){

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let interceptRequest = req.clone({
      setHeaders : {
        'Authorization' : `${this.authService.getToken()}`
      }
    });
    return next.handle(interceptRequest).pipe(catchError(x => this.handleErrors(x)));
  }

  private handleErrors(err: HttpErrorResponse): Observable<any> {
    if (err.status === 401 || err.status === 403) {
      //this.authService.redirectToUrl = this.router.url;
      this.router.navigate(['/login']);
      return of(err.message);
    }
  }
}
