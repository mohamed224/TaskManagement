import {Component, OnInit} from '@angular/core';
import {Credentials} from '../shared/model/credentials';
import {AuthenticationService} from '../shared/service/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials : Credentials = new Credentials();
  constructor(public authService : AuthenticationService , private router : Router) { }

  ngOnInit() {
  }

  login(){
    this.authService.login(this.credentials)

  }

  onRegister(){
    this.router.navigateByUrl('/register')
  }

}
