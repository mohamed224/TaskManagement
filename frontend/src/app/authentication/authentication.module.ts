import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtTokenInterceptor} from './jwtTokenInterceptor';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [LoginComponent],

  providers: [{
    provide : HTTP_INTERCEPTORS,
    useClass : JwtTokenInterceptor,
    multi : true
  }]
})
export class AuthenticationModule { }
