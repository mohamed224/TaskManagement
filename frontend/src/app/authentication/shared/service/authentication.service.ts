import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Credentials} from '../model/credentials';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

   apiUrl = "http://localhost:8083/api/"

  constructor(private http : HttpClient ,private router : Router) { }

  public login(credentials : Credentials){
    return this.http.post<any>(this.apiUrl + 'login',JSON.stringify( credentials),{observe : 'response'})
      .subscribe(resp =>{
        this.saveToken(resp.headers.get('Authorization'))
        localStorage.setItem('username' , credentials.username)
        this.router.navigateByUrl('/tasks')
      },err=>{
        console.log(err.message)
        this.logout()
      })
  }



  public saveToken(token : string){
     localStorage.setItem("token" , token)
  }

  public getToken() : string{
     return localStorage.getItem("token")
  }
  public logout(): void {
     localStorage.removeItem("token")
    localStorage.removeItem('username')
     this.router.navigateByUrl("/login")
  }

  public isLogged() : boolean{
     return !! this.getToken()
  }


}
